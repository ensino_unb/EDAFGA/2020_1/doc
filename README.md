# Plano da Disciplina - Estrutura de Dados (193704)

## Professores
* Fabricio Ataides Braz
* Nilton Correia da Silva
* Sebastião Junio Menezes Campos

## Período
1º Semestre de 2.020

## Turmas
1

## Ementa
- Recursividade 
- Ponteiros e alocação dinâmica de memória 
- Estruturas lineares. Arrays. Listas. Filas. Pilhas 
- Introdução à Complexidade computacional e notação Big-O 
- Algoritmos de busca 
- Algoritmos de ordenação O(nˆ2) 
- Algoritmos em árvores binárias 
- Organização de arquivos 
- Aplicações 

## Método

Independente do método de ensino, a programação de computador é uma habilidade, cuja apreensão exige experimentos contínuos de exercício dos fundamentos da programação. Várias abordagens servem ao própósito de motivar o aluno a buscar esse conhecimento. Neste semestre decidimos aplicar o método de aprendizado baseado em projeto. 

Uma das principais mudanças que aprendizado baseado em projeto traz é que o foco sai da instrução, em que o professor em sala de aula instrui o aluno sobre conceitos e ferramentas, para a investigação, em que o aluno é desafiado a pesquisar conceitos, técnicas e ferramentas para conseguir alcançar os objetivos do projeto que ele se comprometeu a desenvolver. A perspectiva do professor muda da **instrução**, para a **tutoria**, no que diz respeito ao ensino. A perspectiva do aluno muda de **passiva** para **ativa**, no que diz respeito ao aprendizado.

A disciplina prevê um total de 60 horas de formação, a serem distribuídas da seguinte maneira:

| Horas | Atividade                          | Natureza |
|-------|------------------------------|---------|
| 14   | Acolhimento e nivelamento              | Mista |
| 14   | Tutorias                                | Sincrona |
| 18   | Atividades em Grupo                    | Assincrona |
| 1    | Planejamento                           | Assincrona |
| 12    | Seminários                           | Sincrona |

Serão constituídos grupos em que o aluno, apoioado por até 3 (cinco) tutores (professores e monitores), percorrerá uma trilha de aprendizagem envolvendo o desenvolvimento de soluções que demandem as estruturaas de dados clássicas, relacionadas na ementa.

## Ferramentas & Materiais
* [Teams](https://teams.microsoft.com/l/team/19%3a7c375f8bffb04d25aa81d5505fad8c8c%40thread.tacv2/conversations?groupId=f47d7b82-648c-4a60-a235-0cdc1b2b5c35&tenantId=ec359ba1-630b-4d2b-b833-c8e6d48f8059) - Comunicação e trabalho colaborativo;
* [Trello](https://trello.com/b/PRQUXgIo/project-based-learning/fabriciobraz/recommend) - gerência do projeto;
* Python - Linguagem de programação;
* [Gitlab](https://gitlab.com/ensino_unb/am/2020_1) - Reposotório de código e coloboração;
* [Forum de Discussão](https://forum.ailab.unb.br)

## Avaliação

Para que o aluno seja aprovado na disciplina ele deve possuir desempenho superior ou igual a 50, correspondente a menção MM. Além disso, seu comparecimento deve ser superior ou igual a 75% dos eventos estabelecidos pela disciplina. 

### Desempenho

A avaliação de desempenho é resultado da avaliação pelos professores do resultado do grupo, dado o projeto, juntamente com a avaliação individual do aluno pelos membros do grupo.

* AGP: avaliação do grupo pelos professores. Esta avaliação acontece de acordo com os marcos estabelecidos no template de projeto do Trello compatilhado. No dia da apresentação será sorteado um membro do grupo que deverá ter a capacidade de responder pelo projeto.

* AIG: avaliação individual pelos membros do grupo.

![equation](https://latex.codecogs.com/gif.latex?%5Cdfrac%7B%7B%5Csqrt%7B%7BAGP_1%2AAIG_1%7D%7D%7D%2B%202%2A%7B%5Csqrt%7B%7BAGP_2%2AAIG_2%7D%7D%7D%2B%7B%5Csqrt%7B%7BAGP_3%2AAIG_3%7D%7D%7D%7D%7B4%7D)

Serão três encontros avaliativos, observando os seguintes apectos do projeto:
1. Design (peso 1)
2. Desenvolvimento (peso 2)
3. Lançamento (peso 1)

As fases e datas encontram-se detalhadas no Trello. 

### Comparecimento

As atividades síncronas serão consideradas para contabilizar a presença.

## Dinâmica no Grupo
Os grupos serão formados com a quantidade 6 alunos. Em cada iteração, ou seja, períodos entre os encontros avaliativos, deverão ser designados no grupo o `líder`, responsável por conduzir o time na execução das metas do período, além de reportar aos professores desvios e problemas de seus membros.

### Equilíbrio
Para que o grupo não seja prejudicado por eventuais desvios de seus membros, os alunos que não alcancarem nota igual ou superior a 5 na AIG serão desprezados do sorteio para as apresentações.

### Evasão
Grupos com menos de 4 alunos, terão seus membros distribuidos em outros grupos.

## Referências Bibliográficas

* Básica: 

    * (eBrary) BALDWIN, D.; SCRAGG, G. Algorithms and Data Structures: The Science of Computing, 1st ed. Charles River Media, 2004. 
    * LAFORE, R. Estruturas de Dados e Algoritmos em Java, 1a. ed. Ciência Moderna, 2005. 
    * DROZDEK FERRAZ, Inhaúma Neves. Programação com arquivos. Barueri, SP: Manole, 2003. xvii, 345 p. ISBN 8520414893 

* Complementar: 
    * (eBrary) MEHLHORN, K; SANDERS, P. Algorithms and Data Structures: The Basic ToolBox, 1st. ed. Springer, 2008. 
    * (open access) AHO, A. V.; ULLMAN, J. D. Foundations of Computer Science: C Edition (Principles of Computer Science Series), 1st ed., W. H. Freeman, 1994. 
    * GUIMARÃES, A. M.; LAGES. N. A. C. Algoritmos e Estruturas de Dados, 1a. ed. LTC, 1994. 
    * SHERROD, A. Data Structures and Algorithms for Game Developers, 5th ed. Course Technology, 2007. 
    * (eBrary) DESHPANDE, P. S.; KAKDE, O. G. C and Data Structures, 1st ed. Charles River Media, 2004. 
    * (eBrary) DAS, V. V., Principles of Data Structures Using C and C++, 1s ed. New Age International, 2006. 
    * Rance D. Necaise. Data Structures and Algorithms Using Python, 2010.
    * [Python Data Structures](https://www.upgrad.com/blog/data-structures-algorithm-in-python/)
